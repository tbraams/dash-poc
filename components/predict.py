import os
import json

import requests

API_URL = "https://api-inference.huggingface.co/models/cardiffnlp/twitter-roberta-base-sentiment"
headers = {"Authorization": f"Bearer {os.environ.get('API_KEY')}"}


def query(payload):
    """Labels: 0 -> Negative; 1 -> Neutral; 2 -> Positive"""
    data = json.dumps(payload)
    response = requests.request("POST", API_URL, headers=headers, data=data)
    return json.loads(response.content.decode("utf-8"))


def get_prediction(text):
    data = query(text)
    sentiment_strings = []
    for prediction in data:
        string = ""
        for label in prediction:
            if label["label"] == "LABEL_0":
                string += f"Negative: {label['score']} "
            if label["label"] == "LABEL_1":
                string += f"Neutral: {label['score']} "
            if label["label"] == "LABEL_2":
                string += f"Positive: {label['score']} "
        sentiment_strings.append(string)
    return sentiment_strings
